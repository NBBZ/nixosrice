# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      /etc/nixos/hardware-configuration.nix
    ];

  boot.loader = {
    efi.canTouchEfiVariables = true;
    efi.efiSysMountPoint = "/boot";
    grub = {
      enable = true;
      efiSupport = true;
      version = 2;
      gfxmodeEfi = "1920x1080";
      useOSProber = true;
      devices = [ "nodev" ];
    };
  };

  # Use the latest kernel
  boot.kernelPackages = pkgs.linuxPackages_latest;

  # Define hostname.
  networking.hostName = "nbbznixos";

  # Set your time zone.
  time.timeZone = "Asia/Jerusalem";

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.enp7s0.useDHCP = true;

  # Use NetworkManager
  networking.networkmanager.enable = true;

  # Enable fstrim
  services.fstrim.enable = true;
  
  # Configure installed fonts
  fonts.fonts = with pkgs; [
    (nerdfonts.override { fonts = [ "Mononoki" ]; })
    pkgs.noto-fonts-cjk # chinese/japanese/korean
    pkgs.culmus # hebrew
  ];

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };
  
  # Configure X11
  services.xserver = {
    enable = true;
    windowManager.spectrwm.enable = true;
    displayManager.startx.enable = true;
    # Use the proprietary nvidia driver
    videoDrivers = [ "nvidia" ];
  };

  # Add OpenGL 32-bit support
  hardware.opengl.driSupport32Bit = true;

  # Enable sound
  sound.enable = true;
  hardware.pulseaudio.enable = true;

  # Define a user account.
  users.users.nbbz = {
    isNormalUser = true;
    password = "nbbz";
    extraGroups = [ "wheel" "libvirtd" ];
  };

  # Based doas :(
  security.sudo.enable = true;
  security.doas.enable = false;

  # Flatpak setup
  services.flatpak.enable = true;
  xdg.portal.enable = true;
  xdg.portal.extraPortals = [ pkgs.xdg-desktop-portal ];

  # Install packages
  environment.systemPackages = with pkgs; [
    neovim
    numlockx
    zoxide
    devour 
    any-nix-shell
    zathura
    dmenu
    nitrogen
    xkblayout-state
    scrot
    appimage-run
    pulsemixer
    gnome.gnome-calculator
    fzf
    xlockmore
    playerctl
    brave
    ntfs3g
    discord
    xorg.setxkbmap
    qbittorrent
    libarchive
    spotify
    rofi
    moc
    htop
    tdesktop
    git
    neofetch
    materia-theme
    alacritty
    file
    zsh
    #virt-manager
    dunst
    lsd
    mpv
    qutebrowser
    zoom-us
    pciutils
    usbutils
    steam
    python3
    python310
    bc
    lf
    pcmanfm
    lxappearance
    papirus-icon-theme
    syncplay
    pinta
    noisetorch
    nsxiv
  ];

  # Picom
  services.picom = {
    enable = true;
    vSync = false;
  };

  # Enable virt manager
  #virtualisation.libvirtd.enable = true;
  #programs.dconf.enable = true;

  # zsh configuration
  users.defaultUserShell = pkgs.zsh;
  programs.zsh = {
    promptInit = ''
      any-nix-shell zsh --info-right | source /dev/stdin
    '';
    enable = true;
    syntaxHighlighting.enable = true;
    autosuggestions.enable = true;
    histFile = "$HOME/.config/zsh/history";
    shellAliases = {
      s = "sudo";
      sx = "startx ~/.config/x11/xinitrc";
      cl = "clear";
      xr = "xrandr";
      g = "git";
      vim = "nvim";
      ivm = "nvim";
      svim = "sudoedit";
      fv = "du -a ~/.config ~/nixosrice | awk '{print \$2}' | fzf | xargs -r nvim";
      ls = "lsd --long";
      lt = "lsd --all --tree";
      la = "lsd";
      cp = "cp -i";
      mv = "mv -i";
      rm = "rm -i";
      du = "du -h";
      df = "df -h";
      mkdir = "mkdir -pv";
      sd = "shutdown now";
      rb = "reboot";
      py = "python3";
      zz = "z ..";
      clp = "colorpicker";
      speedtest = "librespeed-cli";
      ex = "extract";
      zat = "devour zathura";
      mpv = "devour mpv";
      sxiv = "devour nsxiv";
      pinta = "devour pinta";
     };
  };

  # clean /tmp on boot
  boot.cleanTmpDir=true;

  # Enable ADB
  programs.adb.enable = true;

  # Enable Java
  programs.java.enable = true;

  # Allow non-root users to specify the allow_other or allow_root mount options
  programs.fuse.userAllowOther = true;

  # Define session variables
  environment = {
    sessionVariables = {
      EDITOR = [ "nvim" ];
      VISUAL = [ "nvim" ];
      TERM = [ "alacritty" ];
      PATH = [ "$PATH:$HOME/Scr:$HOME/.local/bin" ];
      ZDOTDIR = [ "$HOME/.config/zsh" ];
      XDG_CONFIG_HOME = [ "$HOME/.config" ];
      XDG_DATA_HOME = [ "$HOME/.local/share" ];
      XDG_CACHE_HOME = [ "$HOME/.cache" ];
      GTK2_RC_FILES = [ "$XDG_CONFIG_HOME/gtk-2.0/gtkrc-2.0" ];
      LESSHISTFILE = [ "-" ];
      GNUPGHOME = [ "$XDG_DATA_HOME/gnupg" ];
      WGETRC = [ "$XDG_CONFIG_HOME/wget/wgetrc" ];
      WINEPREFIX = [ "$XDG_DATA_HOME/wineprefixes/default" ];
      CARGO_HOME = [ "$XDG_DATA_HOME/cargo" ];
      GOPATH = [ "$XDG_DATA_HOME/go" ];
      NVIM_LOG_FILE = [ "$XDG_DATA_HOME/nvim/nvim.log" ];
      QT_QPA_PLATFORMTHEME = [ "qt5ct" ];
    };
  };

  # Config packages
  nixpkgs.config = {
    # Enable unfree licenses
    allowUnfree = true;
    # Enable widevine for chromium
    chromium = {
      enableWideVine = true;
    };
  };

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  programs.mtr.enable = true;
  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
  };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = false;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.11"; # Did you read the comment?

}
