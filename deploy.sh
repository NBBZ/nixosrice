#!/usr/bin/env bash
DIR=$HOME/nixosrice/
CONFIGDEST=$HOME/nixosrice/config

mkdir -p $HOME/.config 2>&1 > /dev/null

link(){
	ln -s -i $CONFIGDEST/$1 $HOME/.config/ 
}

config_files=("alacritty" "rofi" "spectrwm" "mpv" "moc" "lf" "lsd" "zathura" "cava" "dunst" "qutebrowser" "x11" "zsh" "nvim")

for file in ${config_files[@]}
do
	link $file
done

sudo ln -s -i $DIR/system/configuration.nix /etc/nixos/configuration.nix
