#      ___                                       ___     
#     /\  \         _____         _____         /\__\    
#     \:\  \       /::\  \       /::\  \       /::|  |   
#      \:\  \     /:/\:\  \     /:/\:\  \     /:/:|  |   
#  _____\:\  \   /:/ /::\__\   /:/ /::\__\   /:/|:|  |__ 
# /::::::::\__\ /:/_/:/\:|__| /:/_/:/\:|__| /:/ |:| /\__\
# \:\~~\~~\/__/ \:\/:/ /:/  / \:\/:/ /:/  / \/__|:|/:/  / zsh config
#  \:\  \        \::/_/:/  /   \::/_/:/  /      |:/:/  / 
#   \:\  \        \:\/:/  /     \:\/:/  /       |::/  /  
#    \:\__\        \::/  /       \::/  /        |:/  /   
#     \/__/         \/__/         \/__/         |/__/    

# Colors
autoload -U colors && colors

# Prompt
PS1="%B%{$fg[cyan]%}%~%{$fg[magenta]%} ❯ "

export VISUAL="nvim"
export EDITOR="nvim"
export TERM="alacritty"

# Basic auto/tab complete
autoload -U compinit
zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}'
zmodload zsh/complist
compinit
_comp_options+=(globdots)

# vi mode
bindkey -v
export KEYTIMEOUT=1

# Change cursor shape for different vi modes {{{
function zle-keymap-select {
  if [[ ${KEYMAP} == vicmd ]] ||
     [[ $1 = 'underline' ]]; then
    echo -ne '\e[3 q'
  elif [[ ${KEYMAP} == main ]] ||
       [[ ${KEYMAP} == viins ]] ||
       [[ ${KEYMAP} = '' ]] ||
       [[ $1 = 'block' ]]; then
    echo -ne '\e[0 q'
  fi
}
zle -N zle-keymap-select
zle-line-init() {
    echo -ne "\e[0 q"
}
zle -N zle-line-init
echo -ne '\e[0 q' # Use block shape cursor on startup.
# }}}

# Extract function (ex) {{{
SAVEIFS=$IFS
IFS=$(echo -en "\n\b")
function extract {
 if [ -z "$1" ]; then
    # display usage if no parameters given
    echo "Usage: extract <path/file_name>.<zip|rar|bz2|gz|tar|tbz2|tgz|Z|7z|xz|ex|tar.bz2|tar.gz|tar.xz>"
    echo "       extract <path/file_name_1.ext> [path/file_name_2.ext] [path/file_name_3.ext]"
 else
    for n in "$@"
    do
      if [ -f "$n" ] ; then
          case "${n%,}" in
            *.cbt|*.tar.bz2|*.tar.gz|*.tar.xz|*.tbz2|*.tgz|*.txz|*.tar)
                         tar xvf "$n"       ;;
            *.lzma)      unlzma ./"$n"      ;;
            *.bz2)       bunzip2 ./"$n"     ;;
            *.cbr|*.rar)       unrar x -ad ./"$n" ;;
            *.gz)        gunzip ./"$n"      ;;
            *.cbz|*.epub|*.zip)       unzip ./"$n"       ;;
            *.z)         uncompress ./"$n"  ;;
            *.7z|*.arj|*.cab|*.cb7|*.chm|*.deb|*.dmg|*.iso|*.lzh|*.msi|*.pkg|*.rpm|*.udf|*.wim|*.xar)
                         7z x ./"$n"        ;;
            *.xz)        unxz ./"$n"        ;;
            *.exe)       cabextract ./"$n"  ;;
            *.cpio)      cpio -id < ./"$n"  ;;
            *.cba|*.ace)      unace x ./"$n"      ;;
            *)
                         echo "extract: '$n' - unknown archive method"
                         return 1
                         ;;
          esac
      else
          echo "'$n' - file does not exist"
          return 1
      fi
    done
fi
}
IFS=$SAVEIFS
# }}}

# Countdown and stopwatch {{{
function countdown(){
   date1=$((`date +%s` + $1)); 
   while [ "$date1" -ge `date +%s` ]; do 
     echo -ne "$(date -u --date @$(($date1 - `date +%s`)) +%H:%M:%S)\r";
     sleep 0.1
   done
}
function stopwatch(){
  date1=`date +%s`; 
   while true; do 
    echo -ne "$(date -u --date @$((`date +%s` - $date1)) +%H:%M:%S)\r"; 
    sleep 0.1
   done
}
#}}}

# Zoxide
eval "$(zoxide init zsh)"

# vim:foldmethod=marker
